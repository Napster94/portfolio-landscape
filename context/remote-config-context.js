import { createContext } from 'react'

const RemoteConfigContext = createContext()

export default RemoteConfigContext