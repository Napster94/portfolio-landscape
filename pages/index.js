import Headfile from '../components/head-file';
import { useState, useEffect } from 'react';
import Image from 'next/image';
import Loader from 'react-loader';
import NavBar from '../components/navbar';
import Carousel from 'react-bootstrap/Carousel'

export default function Home() {

  const [isReady, setReady] = useState(true);


  return (
    <Loader loaded={isReady}>
      <Headfile pageTitle="Mountain" />
      <NavBar
        pageName="index"
      />
      <div className='row mx-0 bg-primary justify-content-center' style={{ height: '100vh', paddingTop: '88px' }}>
        <Image
          layout="fill"
          objectFit="cover"
          quality={100}
          src={'/img/bg/hero-bg_3.jpg'}
          alt={'Hero Background'}
        />
        <div className="col-12 px-0 my-auto">
          <div className="row mx-0 my-auto text-center">
            <h1 className="col-12 px-0 display-1 text-secondary text-uppercase font-weight-bold">a really tall <br /> mountain </h1>
          </div>
        </div>
        <div className="row mx-0 mt-auto mb-3">
          <div className="col-3 mx-auto">
            <a href="#explore">
              <img src="/img/scroll-arrow.svg" className="w-100" alt="Go to explore" style={{ width: '100%', height: '100%' }} />
            </a>
          </div>
        </div>
      </div>

      <div style={{ background: "#604028" }} id="explore">
        <div className="row mx-0 pt-5 text-center">
          <h2 className="mx-auto mb-0 text-secondary text-capitalize"><span className="text-white"> mountain </span> history</h2>
        </div>
        <div className="row mx-auto">
          <div className="col-lg-6 mx-auto">
            <div className="row mx-0 my-5">
              <p className="col-lg-6 px-3 text-white text-center">
                But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.
              </p>
              <p className="col-lg-6 px-3 text-white text-center">
                But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.
              </p>
            </div>
            <div className="row mx-0 my-5">
              <div className="col-12 px-0">
                <iframe src="https://www.youtube.com/embed/CyfDsNoQLek" frameBorder="0" title="Mountain Hike Video" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen style={{ width: '100%', height: '363px' }}></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='row mx-0 bg-secondary justify-content-center'>
        <div className="col-12 px-0 mb-auto pt-5">
          <Image
            layout="fill"
            objectFit="cover"
            quality={100}
            src={'/img/bg/hero-bg_5.jpg'}
            alt={'Hero Background'}
          />
          <div className="row mx-0 my-auto text-center">
            <h2 className="col-12 px-0 text-secondary text-capitalize"><span className="text-primary">routes</span> to the summit</h2>
          </div>

          <div className="col-lg-6 mx-auto">
            <div className="row mx-0 my-5">
              <p className="col-lg-6 col-9 px-0 text-primary">
                But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.
              </p>
            </div>

            <div className="row mx-0 my-5">
              <button className="col-lg-3 col-6 btn btn-lg btn-primary rounded-pill">Learn More</button>
            </div>
          </div>
        </div>
      </div>

      <div className='row mx-0 justify-content-center' style={{ background: "linear-gradient(#ffffff99, #14213d)" }} id="gallery">
        <div className="col-12 px-0 mb-auto mt-5">
          <div className="row mx-0 my-auto text-center">
            <h2 className="col-12 px-0 text-secondary text-capitalize"><span className="text-primary">photo</span> gallery </h2>
          </div>

          <div className="col-lg-6 mx-auto">
            <div className="row mx-0 my-5">
              <p className="col-12 px-0 text-primary">
                But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.
              </p>
            </div>
          </div>

          <div className="container-fluid mx-auto px-0">
            <div className="row mx-0 my-5">
              <div className="col-lg-3 col-6 mx-auto p-1">
                <Image
                  layout="responsive"
                  width={700}
                  height={475}
                  objectFit="cover"
                  quality={100}
                  src={'/img/bg/hero-bg.jpg'}
                  alt={'Gallery Image'}
                />
              </div>
              <div className="col-lg-3 col-6 mx-auto p-1">
                <Image
                  layout="responsive"
                  width={700}
                  height={475}
                  objectFit="cover"
                  quality={100}
                  src={'/img/bg/hero-bg_3.jpg'}
                  alt={'Gallery Image'}
                />
              </div>
              <div className="col-lg-3 col-6 mx-auto p-1">
                <Image
                  layout="responsive"
                  width={700}
                  height={475}
                  objectFit="cover"
                  quality={100}
                  src={'/img/bg/hero-bg_2.jpg'}
                  alt={'Gallery Image'}
                />
              </div>
              <div className="col-lg-3 col-6 mx-auto p-1">
                <Image
                  layout="responsive"
                  width={700}
                  height={475}
                  objectFit="cover"
                  quality={100}
                  src={'/img/bg/hero-bg.jpg'}
                  alt={'Gallery Image'}
                />
              </div>
              <div className="col-lg-3 col-6 mx-auto p-1">
                <Image
                  layout="responsive"
                  width={700}
                  height={475}
                  objectFit="cover"
                  quality={100}
                  src={'/img/bg/hero-bg_3.jpg'}
                  alt={'Gallery Image'}
                />
              </div>
              <div className="col-lg-3 col-6 mx-auto p-1">
                <Image
                  layout="responsive"
                  width={700}
                  height={475}
                  objectFit="cover"
                  quality={100}
                  src={'/img/bg/hero-bg.jpg'}
                  alt={'Gallery Image'}
                />
              </div>
              <div className="col-lg-3 col-6 mx-auto p-1">
                <Image
                  layout="responsive"
                  width={700}
                  height={475}
                  objectFit="cover"
                  quality={100}
                  src={'/img/bg/hero-bg_2.jpg'}
                  alt={'Gallery Image'}
                />
              </div>
              <div className="col-lg-3 col-6 mx-auto p-1">
                <Image
                  layout="responsive"
                  width={700}
                  height={475}
                  objectFit="cover"
                  quality={100}
                  src={'/img/bg/hero-bg_3.jpg'}
                  alt={'Gallery Image'}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="row mx-0 bg-primary">
        <div className="col-12 px-0 mb-auto mt-5">
          <div className="row mx-0 text-center">
            <h2 className="col-12 px-0 text-white text-capitalize mb-5"><span className="text-secondary">climbers</span> stories </h2>
          </div>

          <div className="container mb-5">

            <Carousel
              indicators={false}
            >
              <Carousel.Item interval={1000}>
                <div className="row mx-0">
                  <div className="col-lg-3 col-6 mx-auto">
                    <div className="card" style={{ background: 'linear-gradient(#ffffff00, #ffffff00, #ffffff80, #ffffffcc, #ffffff, #ffffff)', border: 'none' }}>
                      <div className="w-50 mx-auto" style={{ position: 'relative' }}>
                        <Image
                          layout="responsive"
                          width={500}
                          height={500}
                          objectFit="cover"
                          quality={100}
                          className="rounded-circle"
                          src={'/img/climbers/person-2.jpg'}
                          alt={'Gallery Image'}
                        />
                      </div>
                      <div className="card-body">
                        <h5 className="card-title text-center">Card title</h5>
                        <p className="text-center card-text">But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-3 col-6 mx-auto">
                    <div className="card" style={{ background: 'linear-gradient(#ffffff00, #ffffff00, #ffffff80, #ffffffcc, #ffffff, #ffffff)', border: 'none' }}>
                      <div className="w-50 mx-auto" style={{ position: 'relative' }}>
                        <Image
                          layout="responsive"
                          width={500}
                          height={500}
                          objectFit="cover"
                          quality={100}
                          className="rounded-circle"
                          src={'/img/climbers/person-1.jpg'}
                          alt={'Gallery Image'}
                        />
                      </div>
                      <div className="card-body">
                        <h5 className="card-title text-center">Card title</h5>
                        <p className="text-center card-text">But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-3 col-6 mx-auto">
                    <div className="card" style={{ background: 'linear-gradient(#ffffff00, #ffffff00, #ffffff80, #ffffffcc, #ffffff, #ffffff)', border: 'none' }}>
                      <div className="w-50 mx-auto" style={{ position: 'relative' }}>
                        <Image
                          layout="responsive"
                          width={500}
                          height={500}
                          objectFit="cover"
                          quality={100}
                          className="rounded-circle"
                          src={'/img/climbers/person-3.jpg'}
                          alt={'Gallery Image'}
                        />
                      </div>
                      <div className="card-body">
                        <h5 className="card-title text-center">Card title</h5>
                        <p className="text-center card-text">But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                      </div>
                    </div>
                  </div>

                </div>
              </Carousel.Item>
              <Carousel.Item interval={1000}>
                <div className="row mx-0">
                  <div className="col-lg-3 col-6 mx-auto">
                    <div className="card" style={{ background: 'linear-gradient(#ffffff00, #ffffff00, #ffffff80, #ffffffcc, #ffffff, #ffffff)', border: 'none' }}>
                      <div className="w-50 mx-auto" style={{ position: 'relative' }}>
                        <Image
                          layout="responsive"
                          width={500}
                          height={500}
                          objectFit="cover"
                          quality={100}
                          className="rounded-circle"
                          src={'/img/climbers/person-1.jpg'}
                          alt={'Gallery Image'}
                        />
                      </div>
                      <div className="card-body">
                        <h5 className="card-title text-center">Card title</h5>
                        <p className="text-center card-text">But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-3 col-6 mx-auto">
                    <div className="card" style={{ background: 'linear-gradient(#ffffff00, #ffffff00, #ffffff80, #ffffffcc, #ffffff, #ffffff)', border: 'none' }}>
                      <div className="w-50 mx-auto" style={{ position: 'relative' }}>
                        <Image
                          layout="responsive"
                          width={500}
                          height={500}
                          objectFit="cover"
                          quality={100}
                          className="rounded-circle"
                          src={'/img/climbers/person-3.jpg'}
                          alt={'Gallery Image'}
                        />
                      </div>
                      <div className="card-body">
                        <h5 className="card-title text-center">Card title</h5>
                        <p className="text-center card-text">But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-3 col-6 mx-auto">
                    <div className="card" style={{ background: 'linear-gradient(#ffffff00, #ffffff00, #ffffff80, #ffffffcc, #ffffff, #ffffff)', border: 'none' }}>
                      <div className="w-50 mx-auto" style={{ position: 'relative' }}>
                        <Image
                          layout="responsive"
                          width={500}
                          height={500}
                          objectFit="cover"
                          quality={100}
                          className="rounded-circle"
                          src={'/img/climbers/person-2.jpg'}
                          alt={'Gallery Image'}
                        />
                      </div>
                      <div className="card-body">
                        <h5 className="card-title text-center">Card title</h5>
                        <p className="text-center card-text">But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                      </div>
                    </div>
                  </div>

                </div>
              </Carousel.Item>
            </Carousel>
          </div>
        </div>
      </div>

      <div className="row mx-0" style={{ background: "linear-gradient(#14213d, #000000)" }} >
        <div className="col-12 px-0 mb-auto mt-5">
          <div className="row mx-0 text-center">
            <h2 className="col-12 px-0 text-white text-capitalize mb-5"><span className="text-secondary"> our </span> sponsors </h2>
          </div>

          <div className="container-fluid px-0 mb-5">

            <Carousel
              indicators={false}
              controls={false}
            >
              <Carousel.Item interval={1000}>
                <div className="row mx-0">
                  <div className="col-lg-2 col-6 px-5 mx-auto">
                    {/* <img className="card-img-top w-100 mx-auto" src="/img/sponsors/logo_1.png" alt="Card image cap" /> */}
                    <Image
                      layout="responsive"
                      width={700}
                      height={300}
                      objectFit="cover"
                      quality={100}
                      src={'/img/sponsors/logo_1.png'}
                      alt={'Gallery Image'}
                    />
                  </div>
                  <div className="col-lg-2 col-6 px-5 mx-auto">
                    {/* <img className="card-img-top w-100 mx-auto" src="/img/sponsors/logo_2.png" alt="Card image cap" /> */}
                    <Image
                      layout="responsive"
                      width={700}
                      height={300}
                      objectFit="cover"
                      quality={100}
                      src={'/img/sponsors/logo_2.png'}
                      alt={'Gallery Image'}
                    />
                  </div>
                  <div className="col-lg-2 col-6 px-5 mx-auto">
                    {/* <img className="card-img-top w-100 mx-auto" src="/img/sponsors/logo_3.png" alt="Card image cap" /> */}
                    <Image
                      layout="responsive"
                      width={700}
                      height={300}
                      objectFit="cover"
                      quality={100}
                      src={'/img/sponsors/logo_3.png'}
                      alt={'Gallery Image'}
                    />
                  </div>
                  <div className="col-lg-2 col-6 px-5 mx-auto">
                    {/* <img className="card-img-top w-100 mx-auto" src="/img/sponsors/logo_4.png" alt="Card image cap" /> */}
                    <Image
                      layout="responsive"
                      width={700}
                      height={300}
                      objectFit="cover"
                      quality={100}
                      src={'/img/sponsors/logo_4.png'}
                      alt={'Gallery Image'}
                    />
                  </div>
                </div>
              </Carousel.Item>
            </Carousel>
          </div>
        </div>
      </div>
    </Loader>
  )
}
