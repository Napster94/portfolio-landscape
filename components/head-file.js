import Head from 'next/head'

export default function Headfile(props) {
    return (
        <Head>
            <meta charSet="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <meta name="description" content="Website for my portfolio - Napster94" />
            <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

            {/* <meta httpEquiv="Pragma" content="no-cache" />
            <meta http-equiv="Expires" content="-1" />
            <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" /> */}

            <title>{props.pageTitle || process.env.NEXT_PUBLIC_TITLE}</title>
        </Head>
    )
}