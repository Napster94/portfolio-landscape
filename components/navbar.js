import { useEffect, useState } from "react";
import TopNav from '../components/top-nav';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { useMediaQuery } from 'react-responsive';


export default function NavBar(props) {

    const isDesktopOrLaptop = useMediaQuery({ minWidth: 992 });
    const isTabletOrMobile = useMediaQuery({ maxWidth: 991 });

    return (
        <TopNav
            bgColor={'transparent'}
            shadow={false}
            scrollColor={'#fca311'}
            className={'py-4'}
            name={'Mountain Mountain'}
            right={
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#explore">Explore</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#gallery">Gallery</a>
                    </li>
                </ul>
            }
            mobileNav={
                <ul className="navbar-nav text-center">

                    <li className="nav-item my-3 active">
                        <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item my-3">
                        <a className="nav-link" href="#explore">Explore</a>
                    </li>
                    <li className="nav-item my-3">
                        <a className="nav-link" href="#gallery">Gallery</a>
                    </li>
                </ul>
            }
        ></TopNav >
    )
}