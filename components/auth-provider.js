import AuthContext from '../context/auth-context';
import { useState, useEffect } from 'react'
import Loader from 'react-loader';
import firebase from 'firebase/app';
import 'firebase/auth';

export function AuthProvider(props) {
    const [user, setUser] = useState(null);
    const [token, setToken] = useState(null);

    // handle auth logic here...
    useEffect(() => {
        return firebase.auth().onIdTokenChanged(async (user) => {
            if (!user) {
                setUser(null);
                // nookies.set(undefined, 'token', '');
            } else {
                let userToken = await user.getIdToken();
                setUser(user);
                setToken(userToken);
                // nookies.set(undefined, 'token', token);
            }
        });
    }, []);

    // force refresh the token every 10 minutes
    useEffect(() => {
        const handle = setInterval(async () => {
            const user = firebaseClient.auth().currentUser;
            if (user) await user.getIdToken(true);
        }, 10 * 60 * 1000);

        // clean up setInterval
        return () => clearInterval(handle);
    }, []);

    return (
        <AuthContext.Provider value={{ user: user, token: token }}>
            {props.children}
        </AuthContext.Provider>
    );
}